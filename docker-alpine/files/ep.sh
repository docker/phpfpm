#!/bin/bash

echo "[$0] running with id $(whoami)..." > /dev/stderr

PREFIX_RUN_CMD="source"

# Test de présence de sudo (images de DEV seulement)
if [ "true" = "${USE_SUDO}" ] && [ -x /usr/bin/sudo ];
then
  PREFIX_RUN_CMD="sudo -E"
fi

# Preparation PHP ... + whatever the app adds
for S in /ep.d/*.sh
do
    ${PREFIX_RUN_CMD} $S
done

# Lancement du processus d'avant plan 
exec "$@"
