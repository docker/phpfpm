### PHP via packages IUS / CentOS 7
ARG BASE_IMAGE
ARG BASE_IMAGE_VERSION
FROM ${BASE_IMAGE}:${BASE_IMAGE_VERSION}
ARG PHPVERSION

RUN echo -e "Building PHP $PHPVERSION on $(cat /etc/centos-release)"

# Spécifiques à cet image
LABEL Vendor="Actilis" \
      Maintainer="Francois MICAUX <dok-images@actilis.net>" \
      License=GPLv3 

# ===============================================
# Layer de base
# à reprendre par toutes les images pour mutualisation
RUN set -eux \
 && dnf -y install epel-release \
 && dnf -y install https://rpms.remirepo.net/enterprise/remi-release-9.rpm \
 && dnf -y install ca-certificates ssmtp glibc-locale-source \
 && dnf -y module switch-to php:remi-8.2 \
 && dnf -y upgrade \
 && dnf clean all && rm -rf /var/cache/dnf

ENV WEB_USERNAME www-data
ENV WEB_GROUPNAME www-data

# Ensure Username and Groupname are present
RUN set -eux \
 && sed -i '/:33:/d' /etc/group \
 && grep -q ${WEB_GROUPNAME} /etc/group || groupadd -g ${WEB_GID:=33} -r ${WEB_GROUPNAME}

RUN set -eux \
 && sed -i '/:33:/d' /etc/passwd \
 && grep -q ${WEB_USERNAME} /etc/passwd || useradd -u ${WEB_UID:=33} -g ${WEB_GROUPNAME} ${WEB_USERNAME}

# Add PHP common extensions : see embeded script
COPY files/install-php+deps.sh  /usr/local/bin/install-php+deps.sh
RUN set -eux \
 && /usr/local/bin/install-php+deps.sh 

# ===============================================
# Install Composer
COPY files/install-composer.sh  /usr/local/bin/install-composer.sh
RUN set -eux \
 && /usr/local/bin/install-composer.sh 



# env vars nécessaires pour les scripts d'EP
ENV PHPFPM_POOL_CONFIG /etc/php-fpm.d/www.conf

## Ports
EXPOSE 9000

# Déclaration après le peuplement sinon : données invisibles dans les surcouches

## Entrypoint calls all scripts in /ep.d/*.sh (they apply the configuration at statup)
ENTRYPOINT ["/ep.sh"]
## At the end, ep.sh does "exec $@" (CMD)
CMD ["/usr/sbin/php-fpm", "-F"]

# Test de vie
#HEALTHCHECK --interval=5s --timeout=2s --retries=3 CMD /usr/bin/php /var/www/html/.alive.php | grep -q ^OK
#COPY --chown=${WEB_USERNAME}:${WEB_GROUPNAME} files/alive.php /var/www/html/.alive.php

## Default dir for apps
WORKDIR /var/www/html

ENV LANG=fr_FR.UTF-8
RUN localedef -i fr_FR -f UTF-8 fr_FR.UTF-8 \
 && localedef -i en_US -f UTF-8 en_US.UTF-8

# ==========================================
RUN mkdir -p -m 755 /ep.d
COPY files/ep-system.sh    /ep.d/00-system.sh
COPY files/ep-php.sh       /ep.d/01-php.sh
COPY files/ep-newrelic.sh  /ep.d/02-newrelic.sh
COPY files/ep.sh           /ep.sh
## Données à importer (ADD, COPY,...)
COPY files/phpfpm-pool-www.conf    ${PHPFPM_POOL_CONFIG}

# Manage permisisons, clean...
RUN chmod -R 755 /ep.sh /ep.d/ /etc/php-fpm.d/www.conf

