#!/bin/bash

echo "== Installation PHPVERSION: $PHPVERSION"


# Packages names have changed on IUS for PHP73
PHP=php
case ${PHPVERSION} in 
  8.4*) REPO=remi-8.4
       ;;
  8.3*) REPO=remi-8.3
       ;;
  8.2*) REPO=remi-8.2
       ;;
  8.1*) REPO=remi-8.1
       ;;

  *)   echo "Erreur Version PHP" && exit 1
esac

dnf -y module switch-to php:$REPO

# Add Base packages
set -eux \
 && dnf makecache && dnf -y install \
    ${PHP}-bcmath \
    ${PHP}-gd \
    ${PHP}-intl \
    ${PHP}-json \
    ${PHP}-ldap \
    ${PHP}-mbstring \
    ${PHP}-mysqlnd \
    ${PHP}-opcache \
    ${PHP}-pgsql \
    ${PHP}-soap \
    ${PHP}-xml \
    ${PHP}-cli \
    ${PHP}-common \
    ${PHP}-fpm

set -eux \
 && dnf -y install \
    php-pecl-zip.x86_64 \
    php-pecl-redis6.x86_64


# Clean
dnf clean all && rm -rf /var/cache/dnf

# Permisisons (ouverture uniquement sur surcharge USER)
chmod -R 777 /var/log/php-fpm
