#!/bin/bash

INI_FILENAME=/etc/php.d/newrelic.ini

#- **NEWRELIC_ENABLE** (false par défaut) : démarrer l'agent si **true** (`newrelic-install install`) ou pas.
#- **NEWRELIC_LICENCE_KEY** (vide par défaut) : clé de licence.

# Activation ou pas
if [ "${NEWRELIC_ENABLE:-false}" = "true" ] ; then

   if [ "${NEWRELIC_DO_INSTALL:-false}" = "true" ] ; then
      NR_INSTALL_SILENT=1 newrelic-install install
   fi

   # enabled + licensekey
   if [ -f ${INI_FILENAME} ] && [ "{NEWRELIC_LICENSE_KEY:-null}" != "null" ] ; then 
     sed -i \
      -e "/newrelic.enabled/s,^;,," \
      -e "/newrelic.license/s,REPLACE_WITH_REAL_KEY,${NEWRELIC_LICENSE_KEY}," \
      ${INI_FILENAME}
   fi

fi   

unset INI_FILENAME