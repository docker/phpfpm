#!/bin/bash

# Latest
cd /root
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"; 
php -r "\$lines=file('https://composer.github.io/installer.sig',FILE_IGNORE_NEW_LINES); if (  hash_file('SHA384', 'composer-setup.php') ===  \$lines[0] ) { echo 'Installer verified'; } else { echo 'Insta    ller corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"; 
php composer-setup.php --install-dir=/usr/local/bin --1 --filename=composer1; 
php composer-setup.php --install-dir=/usr/local/bin --2 --filename=composer2; 
php -r "unlink('composer-setup.php');"

# Pointage vers la version 1 pour l'instant
ln -s /usr/local/bin/composer1 /usr/local/bin/composer

echo "=====> $(/usr/local/bin/composer --version)"
