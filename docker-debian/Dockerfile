ARG BASE_IMAGE
ARG BASE_IMAGE_VERSION
ARG PHPVERSION
FROM ${BASE_IMAGE}:${PHPVERSION}-${BASE_IMAGE_VERSION}


RUN echo -e "Building from ${BASE_IMAGE}:${PHPVERSION}${BASE_IMAGE_VERSION}"

# Spécifiques à cet image
LABEL Vendor="Actilis" \
      Maintainer="Francois MICAUX <dok-images@actilis.net>" \
      License=GPLv3 

# Update global (Limiter les failles détectées par Trivy) + add packages
RUN set -eux \
 && apt-get update \
 && apt-get -y upgrade \
 && apt-get -y install ssmtp \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
 && chmod 755 /etc/ssmtp


# Capabilities for chown ==> enables "choose-uuid" feature when non-root
RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends libcap2-bin; \
    setcap cap_chown=ep /bin/chown; \
    apt-get remove -y libcap2-bin; \
    rm -rf /var/lib/apt/lists/*;

# Identité du service
ENV WEB_USERNAME www-data
ENV WEB_GROUPNAME www-data

# ==== Paramétrage du POOL FPM -> on écrase le www.conf standard ====
ENV CONFIG_PATH /usr/local/etc
ENV PHPFPM_POOL_CONFIG ${CONFIG_PATH}/php-fpm.d/www.conf
ENV OVERRIDE_CONFIG_FILE zzz-override-tuning.ini
COPY --chown=${WEB_USERNAME}:${WEB_GROUPNAME} files/phpfpm-pool-www.conf    ${PHPFPM_POOL_CONFIG}

# Ensure permissions are OK for root-less
RUN set -eux \
 && touch ${CONFIG_PATH}/${OVERRIDE_CONFIG_FILE} \
 && install -d -o ${WEB_USERNAME} -g ${WEB_GROUPNAME} -m 755 \
    /var/www/html \
    ${CONFIG_PATH}/php-fpm.d \
    ${CONFIG_PATH}/php/conf.d/ \
    /var/log/php \
    /var/lib/php/fpm \
    /var/lib/php/fpm/session \
    /var/lib/php/fpm/wsdlcache \
    /var/lib/php/fpm/opcache \
    /run/php-fpm



# ==== Gestion de l'EP surchargeable =====
# Entrypoint -> scripts in /ep.d/*.sh 
RUN install -d -o ${WEB_USERNAME} -g ${WEB_GROUPNAME} -m 755 /ep.d
COPY --chown=${WEB_USERNAME}:${WEB_GROUPNAME} --chmod=755 files/ep-system.sh    /ep.d/00-system.sh
COPY --chown=${WEB_USERNAME}:${WEB_GROUPNAME} --chmod=755 files/ep-php.sh       /ep.d/01-php.sh
COPY --chown=${WEB_USERNAME}:${WEB_GROUPNAME} --chmod=755 files/ep-newrelic.sh  /ep.d/02-newrelic.sh
COPY --chown=${WEB_USERNAME}:${WEB_GROUPNAME} --chmod=755 files/ep.sh           /ep.sh

# ===============================================
# Import Install-composer-script
COPY files/install-composer.sh  /usr/local/bin/install-composer.sh


## At the end, ep.sh does "exec $@" (CMD)
ENTRYPOINT ["/ep.sh"]
CMD ["/usr/local/sbin/php-fpm", "-F"]

## Default dir for apps
WORKDIR /var/www/html


# Test de vie
#HEALTHCHECK --interval=5s --timeout=2s --retries=3 CMD /usr/bin/php /var/www/html/.alive.php | grep -q ^OK
#COPY --chown=${WEB_USERNAME}:${WEB_GROUPNAME} files/alive.php /var/www/html/.alive.php
