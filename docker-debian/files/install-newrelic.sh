#!/bin/bash

echo "== Installation NewRelic"

### Enable PHP Extensions
apt-get update \
&&  apt-get -y install gnupg


### Add New-Relic repository
set -eux \
&& curl -s https://download.newrelic.com/548C16BF.gpg | apt-key add - \
&& echo 'deb http://apt.newrelic.com/debian/ newrelic non-free' > /etc/apt/sources.list.d/newrelic.list \
&& apt-get update \
&& DEBIAN_FRONTEND=noninteractive apt-get -y install newrelic-php5


### Install new-relic
# NR_INSTALL_SILENT=1 newrelic-install install

# Clean
apt-get -y purge gnupg  \
&& apt-get -y autoremove \
&& apt-get -y clean \
&&  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
