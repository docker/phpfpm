#!/bin/bash

# Test de présence de sudo (images de DEV seulement)
if [ "true" = "${USE_SUDO}" ] && [ -x /usr/bin/sudo ];
then
  PREFIX_RUN_CMD="sudo -E"
  echo "/ep.d/ scripts will bu run with sudo."
fi


# UID
myUID=$(id -u)

# GID : don't stay with GID=0 if UID is given
myGID=$(id -g); [ ${myGID} = 0 ] && myGID=${myUID}

export myUID myGID


# Prise en compte DES scripts d'EP.
for S in /ep.d/*.sh; do
   if [ -x "${S}" ] ; then 
      echo "Running : ${S} (in a sub-shell)"
      ${PREFIX_RUN_CMD} "${S}"
   else
      echo "Sourcing : ${S}"
      . "${S}"
   fi
   # Stop si erreur
   CR=$?
   if [ $CR -ne 0 ] ; then 
      echo "==> CR = $CR"
      [ "true" = "${STOP_ON_EP_ERRORS:-false}" ] && exit 1
   fi 
done


# Lancement du processus d'avant plan 
exec "$@"
