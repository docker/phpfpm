#!/bin/bash

# Latest
cd /root
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"; 
php -r "\$lines=file('https://composer.github.io/installer.sig',FILE_IGNORE_NEW_LINES); if (  hash_file('SHA384', 'composer-setup.php') ===  \$lines[0] ) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"; 

case ${1:-2} in
  1) php composer-setup.php --install-dir=/usr/local/bin --1 --filename=composer
  ;;
  2) php composer-setup.php --install-dir=/usr/local/bin --2 --filename=composer
  ;;
  *) echo "Installation de Composer : Version ${1} non prise en charge"
     exit
  ;;
esac

php -r "unlink('composer-setup.php');"


echo "=====> $(/usr/local/bin/composer --version)"
