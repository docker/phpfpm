#!/bin/bash

INI_FILENAME=/usr/local/etc/php/conf.d/newrelic.ini

# Activation ou pas
if [ "${NEWRELIC_ENABLE:-false}" = "true" ] ; then

   if [ "${NEWRELIC_DO_INSTALL:-false}" = "true" ] ; then
      NR_INSTALL_SILENT=1 newrelic-install install
   fi

   # enabled + licensekey
   if [ -f ${INI_FILENAME} ] && [ "{NEWRELIC_LICENSE_KEY:-null}" != "null" ] ; then 
     sed -i \
      -e "/newrelic.enabled/s,^;,," \
      -e "/newrelic.license/s,REPLACE_WITH_REAL_KEY,${NEWRELIC_LICENSE_KEY}," \
      ${INI_FILENAME}
   fi

fi   

unset INI_FILENAME
