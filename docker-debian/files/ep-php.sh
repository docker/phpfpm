#!/bin/bash

myName=EP-PHP

function _echo()
{
        [ "${DEBUG}" == "true" ] && echo "$@"
}


echo "[$myName] running with UID ${myUID}/${myGID}..." >/dev/stderr

# Enable choose-uuid (WARNING: cap_chown=ep on /bin/chown)
chown -R ${myUID}:${myGID} \
  ${CONFIG_PATH}/php-fpm.d \
  ${CONFIG_PATH}/php/conf.d

# Fix owner to fit with --user 
chown -R ${myUID}:${myGID} /var/log/php /var/lib/php/fpm{,/session,/wsdlcache,/opcache} /run/php-fpm

# Global LOG config
# AccessLog to stdout -> fait dans ${CONFIG_PATH}/php-fpm.d/www.conf
# ErrorLog to stderr  -> fait dans ${CONFIG_PATH}/php-fpm.d/docker.conf
# SlowLog  to stderr  -> fait dans ${CONFIG_PATH}/php-fpm.d/www.conf

# Test d'existance du user défini par "USER_NAME" / "USER_UID"
# Si ces variables existent, on est dans une surcouche de dev
# --> identité = USER_NAME / USER_NAME
# Sinon, on est en mode "classique" 
# --> identité = WEB_USERNAME / WEB_GROUPNAME hérité du Dockerfile
if grep -q "^${USER_NAME}:x:" /etc/passwd; then
	WEB_USERNAME=${USER_NAME}
	WEB_GROUPNAME=$(id -gn ${WEB_USERNAME})
	echo "DEV USER:${WEB_USERNAME} (GROUP:${WEB_GROUPNAME})"
fi

# TUNING (CLI + PHP-FPM)
set | grep "^PHP__" | while read L ; do
 echo "${L#PHP__}" | sed -e 's,_dot_,.,' -e 's,=, = ,' 
done > /tmp/tuning.ini

# Apply PHP config
mv /tmp/tuning.ini ${CONFIG_PATH}/php/conf.d/${OVERRIDE_CONFIG_FILE}

_echo -e "TUNING PHP :" :
[ "${DEBUG}" == true ] && cat ${CONFIG_PATH}/php/conf.d/${OVERRIDE_CONFIG_FILE}

# TUNING POOL PHP-FPM
_echo -e "\n==============="
_echo -e "Tuning PHP-FPM PM:"
_echo -e "==============="
_echo -e "** PM_MODE : ${PM_MODE:=ondemand}"
_echo -e "** PM_MAX_CHILDREN : ${PM_MAX_CHILDREN:=100}"
_echo -e "** PM_MAX_REQUESTS : ${PM_MAX_REQUESTS:=1000}"
_echo -e "** PM_PROCESS_IDLE_TIMEOUT : ${PM_PROCESS_IDLE_TIMEOUT:=60s}"
_echo -e "======================================="

sed -i \
    -e "/^user =/s,WEB_USERNAME,${WEB_USERNAME}," \
    -e "/^group =/s,WEB_GROUPNAME,${WEB_GROUPNAME}," \
    -e "s,PM_MODE,${PM_MODE}," \
    -e "s,PM_MAX_CHILDREN,${PM_MAX_CHILDREN}," \
    -e "s,PM_PROCESS_IDLE_TIMEOUT,${PM_PROCESS_IDLE_TIMEOUT}," \
    -e "s,PM_MAX_REQUESTS,${PM_MAX_REQUESTS}," \
    ${PHPFPM_POOL_CONFIG}

# Si on est pas root, on commente les déclaration d'identité dans le pool (évite un warning)
test $(id -u) != 0 && sed -i \
    -e "/^user =/s,^,;," \
    -e "/^group =/s,^,;," \
     ${PHPFPM_POOL_CONFIG}

# Tuning des processus uniquement si MODE = dynamic
if [ "${PM_MODE}" == "dynamic" ] ; then
  _echo -e "** PM_START_SERVERS : ${PM_START_SERVERS:=10}"
  _echo -e "** PM_MIN_SPARE_SERVERS : ${PM_MIN_SPARE_SERVERS:=5}"
  _echo -e "** PM_MAX_SPARE_SERVERS : ${PM_MAX_SPARE_SERVERS:=25}"
else
  PM_START_SERVERS=0
  PM_MIN_SPARE_SERVERS=0
  PM_MAX_SPARE_SERVERS=0
fi
sed -i \
    -e "s,PM_START_SERVERS,${PM_START_SERVERS}," \
    -e "s,PM_MIN_SPARE_SERVERS,${PM_MIN_SPARE_SERVERS}," \
    -e "s,PM_MAX_SPARE_SERVERS,${PM_MAX_SPARE_SERVERS}," \
    ${PHPFPM_POOL_CONFIG}

# ===========================================
# HTTPD_ENABLE_STATUS is true ==> Activate it
# ===========================================
if [ "${PHPFPM_ENABLE_STATUS}" == true ] ; then
      sed -i -e "/PHPFPM_STATUS_URI/s,^;,," \
             -e "/PHPFPM_STATUS_URI/s,PHPFPM_STATUS_URI,${PHPFPM_STATUS_URI:=/status}," \
        ${PHPFPM_POOL_CONFIG}
fi

# TIMEOUTS :  PHPFPM_SLOWLOG_TIMEOUT & PHPFPM_REQUEST_TIMEOUT
sed -i -e "/PHPFPM_SLOWLOG_TIMEOUT/s,PHPFPM_SLOWLOG_TIMEOUT,${PHPFPM_SLOWLOG_TIMEOUT:=3}," \
       -e "/PHPFPM_REQUEST_TIMEOUT/s,PHPFPM_REQUEST_TIMEOUT,${PHPFPM_REQUEST_TIMEOUT:=300}," \
       ${PHPFPM_POOL_CONFIG}

# Catch logs in parent process (default = no)
sed -i -e "/PHPFPM_CATCH_WORKERS_OUTPUT/s,PHPFPM_CATCH_WORKERS_OUTPUT,${PHPFPM_CATCH_WORKERS_OUTPUT:=no}," \
       ${PHPFPM_POOL_CONFIG}

_echo -e "\nPHP-FPM pool config : (${PHPFPM_POOL_CONFIG})"
[ "${DEBUG}" == true ] && grep -vE '^;|^ *$' ${PHPFPM_POOL_CONFIG}

# si supervisord est présent, on y place notre config
[ -d /etc/supervisord.d ] && echo "
[program:phpfpm]
command=/usr/local/sbin/php-fpm -F
stderr_logfile=/dev/stderr
" > /etc/supervisord.d/phpfpm.ini


# = Fix owner of web-content
[ "${FIX_WEBCONTENT_OWNER}" == "true" ] && chown -R ${myUID}:${myGID} /var/www/html

# = En DEV, si on a pas de contenu, on en crée un.
if [ $(ls -a /var/www/html | wc -w) == 2 ] ; then
  echo "[$myName] /var/www/html is empty, adding an index.php" >/dev/stderr
  chown ${myUID}:${myGID} /var/www/html && echo "<?php printf ('Servi par phpfpm (%s)' , gethostname() ); ?> " > /var/www/html/index.php
fi
