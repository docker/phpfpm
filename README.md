---
title: Images Docker PHPFPM
---

[![pipeline status](https://gitlab.actilis.net/docker-images/phpfpm/badges/master/pipeline.svg)](https://gitlab.actilis.net/docker-images/phpfpm/commits/master)


# Deux types d'image

Deux types d'images sont proposés :
- une base Docker Officielle minimaliste sur variantes Debian et Alpine
- une base AlmaLinux 9 utilisant les packages de Remi COLLET.

L'image basée sur CentOS n'est plus construite depuis le 1er juillet 2024 (fin de vie de CentOS 7), remplacée par une base AlmaLinux 9.

Les deux types d'images intègrent les éléments permettant :
- le choix de l'identité des processus
- le tuning
- la remontée de logs en vue de leur historisation
- la supervision

## Image "Officielle ADEME" Docker PHP-FPM

  - VERSIONS de PHP  : 7.4 (EOL), 8.0, 8.1, 8., 8.3
  - Images de départ VERSION-fpm (base Debian) et VERSION-fpm-alpine

### Extensions présentes au départ dans ces images

#### Base Debian

```ini
[PHP Modules]
Core
ctype
curl
date
dom
fileinfo
filter
ftp
hash
iconv
json
libxml
mbstring
mysqlnd
openssl
pcre
PDO
pdo_sqlite
Phar
posix
random
readline
Reflection
session
SimpleXML
sodium
SPL
sqlite3
standard
tokenizer
xml
xmlreader
xmlwriter
zlib
```

#### Base CentOS

```ini
[PHP Modules]
bcmath
bz2
calendar
Core
ctype
curl
date
dom
exif
fileinfo
filter
ftp
gd
gettext
hash
iconv
igbinary
intl
json
ldap
libxml
mbstring
msgpack
mysqli
mysqlnd
openssl
pcntl
pcre
PDO
pdo_mysql
pdo_pgsql
pdo_sqlite
pgsql
Phar
random
readline
redis
Reflection
session
SimpleXML
soap
sockets
SPL
sqlite3
standard
tokenizer
xml
xmlreader
xmlwriter
xsl
Zend OPcache
zip
zlib

[Zend Modules]
Zend OPcache
```


## Image sur base CentOS

  - Versions de PHP : 8.0 (latest) / 8.1 (latest)
  - Image de départ : CentOS 7.9
  - Packages : REMI


**Attention** : l'image Docker CentOS 7.9 est deprecated. Les builds basés sur CentOS doivent docn être abandonnés.



### Composer

Versions de Composer : latest v1 & latest v2.

Selon la base :

- CentOS, il est présent dans le 2 versions (`/usr/local/bin/composer{1,2}`).
- alpine et debian-docker, il n'est pas installé par défaut. Dans vos Dockerfile il faut exécuter le script `/usr/local/bin/install-composer.sh` (avec éventuellement l'argument `1` pour la V1, sinon, c'est V2 qui sera installée par défaut)

### NewRrelic

La dernière version du package "newrelic-php5" disponible sur le (repository)[http://download.newrelic.com/pub/newrelic/el5/x86_64/] peut être installée (il faut le faire dans votre Dockerfile)

Extension disponible sur les variantes suivantes :
- centos : OK, présent par défaut
- alpine : pas prévu.
- debian : pas installé par défaut, il faut exécuter `/usr/local/bin/install-newrelic.sh` dans vos Dockerfile pour l'intégrer à vos images.




# Utilisation 

## Mode de configuration de PHP

Deux approches existent :

- présenter des fichiers de configuration au bon endroit
- utiliser des variables d'environnement.

Il est fortement conseillé d'utiliser des variables d'environnement (https://12factor.net/fr/config).

### Méthode 1 : Présenter un fichier de configuration au conteneur

**Sur la base officielle** : la configuration peut être déposés sous forme de fichiers ".ini" dans le répertoire **/usr/local/etc/php/conf.d**, comme pour l'image amont, que ce soit pour la base Debian ou Alpine.

**Sur la base CentOS** : la configuration peut être déposée sous forme de fichiers ".ini" dans dossier **/etc/php.d**. 


### Méthode 2 : Utiliser des variables d'environnement

Toute variable d'environnement préfixée par **PHP__** est intégrée en directive de configuration de PHP dans le fichier "**99-tuning.ini**" au démarrage du conteneur.

Ce fichier de configuration "**99-tuning.ini**" est automatiquement ajouté dans **/usr/local/etc/php/conf.d** (base officielle) ou **/etc/php.d** (base CentOS) et donc pris en compte dans tous les cas.

Exemple : 

```
docker container run --rm -it --env PHP__memory_limit=128M  phpfpm:7.3-centos cat /etc/php.d/99-tuning.ini
```

Les variables dont le nom contient un "." (exemple session.save_path) doivent être codées en remplaçant le "." par "**underscore**" "**dot**" "**underscore**" :

Exemple :

    PHP__session_dot_save_path=valeur


Exemple de déclarations :
```shell
$ cat env.txt 
PHP__post_max_size=50M
PHP__memory_limit=256M
PHP__upload_max_filesize=29M
PHP__session_dot_save_path=/var/lib/php/fpm/session
```

### Tuning du pool PHP-FPM

Le pool PHP-FPM hérite des variables définies par **99-tuning.ini**. 

En complément, il a son propre fichier de paramétrage, notamment utilisé que pour gérer l'identité du pool, la localisation des logs et la parie "PM". 

#### Tuning du pool PHP-FPM

- **PM\_MODE** : par défaut "ondemand" (dynamic / static).
- **PM\_MAX\_CHILDREN** : par défaut 100.
- **PM\_MAX\_REQUESTS** : par défaut 500.
- **PM\_PROCESS\_IDLE\_TIMEOUT** : par défaut 30s

Si PM_MODE = dynamic :

- **PM\_START\_SERVERS** : par défaut 2
- **PM\_MIN\_SPARE\_SERVERS** : par défaut 2
- **PM\_MAX\_SPARE\_SERVERS** : par défaut 5

#### Activation du Status

- **PHPFPM\_ENABLE\_STATUS** : désactivé par défaut (**true** pour actuver)
- **PHPFPM\_STATUS\_URI** : "/status" par défaut si activé.


#### Timeouts

Les timeouts suivants sont réglables par les variables ci-dessous :

- **PHPFPM_SLOWLOG_TIMEOUT** (3 secondes par défaut)
- **PHPFPM_REQUEST_TIMEOUT** (300 secondes par défaut)

#### Logs 

Les logs sont émis sur stderr des workers. Si possitionnée à **yes**, la variable suivante permet (c'est contraire à la spec de FastCGI) de les ramener dans le log du processus d'avant-plan :

- **PHPFPM_CATCH_WORKERS_OUTPUT** (**no** par défaut)

> Cela permet par exemple d'utiliser dans le code une fonction de ce type pour loguer :
> ```
>    function stderr_log($message, $prefix = "INFO") {
>        $stderr = fopen('php://stderr', 'w');
>        fwrite($stderr, $prefix . $message . PHP_EOL);
>        fclose($stderr);
>    }
> ```


### Variables pour NewRelic

Les variables suivantes sont disponibles :

- **NEWRELIC_ENABLE** (false par défaut) : activer l'extension PHP

- **NEWRELIC_DO_INSTALL** (false par défaut) : Exécuter `newrelic-install install` ou pas (voir les lignes 125 et suivantes du fichier **newrelic.ini**).

Et pour le fichier de configuration : (Doc)[https://docs.newrelic.com/docs/agents/php-agent/advanced-installation/docker-other-container-environments-install-php-agent/#agent-container]

- **NEWRELIC_LICENSE_KEY** (vide par défaut) : clé de licence, appliquée dans "newrelic.ini" (PHP).


### Variables disponibles uniquement sur la base CentOS

#### Réglages système

  - **LDAP\_TLS\_REQCERT** : valeur affectée à TLS_REQCERT dans /etc/opendldap/ldap.conf (par défaut : **never**)
  - **LANG** : valeur par défaut = **fr_FR.UTF-8**
  - **TIMZEONE** : valeur par défaut = **Europe/Paris**
  
      - Effectif au niveau OS + au niveau de PHP.


#### Pour l'émission de mail

L'idéal est d'utiliser la classe PHPMailer et de la configurer pour utiliser un service SMTP authentifié. Sans cela, il reste possible d'utiliser la commande "sendmail". Le paquet *ssmtp* est installé et la fournit.

  - **SMTP\_RELAY\_HOST** : nom du relayhost = **unconfigured**
  - **SMTP\_RELAY\_PORT** : port du relayhost = **25**
  - **SMTP\_DOMAIN\_NAME** : nom de domaine émetteur par défaut = **localdomain**
  - **SMTP\_REWRITE\_FROM** : Valeur de la directive **FromLineOveride** (NO) par défaut.




## Logs

Ils ne restent pas dans le conteneur, et ceci pour toutes les versions (**CentOS** et **Oficielle**) :

- AccessLog vers stdout
- ErrorLog vers stderr
- SlowLog  vers stderr

Cela représente un changement par rapport aux images officielles PHP qui envoient tout vers stderr.

Ils peuvent ainsi être consultés par **docker container logs** ou transmis à un récepteur tiers.

Le timeout de traçage des requêtes dites "slow" (**request_slowlog_timeout**) est réglable par la variable d'environnement **PHPFPM_SLOWLOG_TIMEOUT** (3 secondes par défaut).


## PHPFPM derrière un reverse-proxy

Les deux fichiers suivants donnent un exemple simplissime d'utilisation de l'image phpfpm derrière l'image httpd.

Ici, l'application est d'abord packagée dans une image intermédiaire, puis intégrée, grâce au même Dockerfile, dans l'image httpd et dans l'image phpfpm (voir l'argument BASE_IMAGE dans `compose.yaml`).

### compose.yaml

```yaml
version: "3.9"

networks:
  app:

services:
  httpd:
    build:
      context: .
      args: 
        BASE_IMAGE: registry.ademe.fr/docker/httpd:2.4-alpine
    restart: always
    hostname: webgw.local
    networks:
    - app
    environment:
      - HTTPD_ENABLE_PHPFPM_INET=true
    ports:
    - "8000:80"

  phpfpm:
    build:
      context: .
      args: 
        BASE_IMAGE: registry.ademe.fr/docker/phpfpm:8.1-debian
    restart: always
    networks:
    - app

```

### Dockerfile

```Dockerfile
ARG BASE_IMAGE
# ======================
# Empaquetage de l'appli
# ======================
FROM alpine:3.13 as appbuilder
# RUN ....
# ADD ...
# COPY ...
RUN mkdir /app 
RUN echo "Servi directement par Apache." > /app/index.html
RUN echo "<?php printf ('Servi par Apache->phpfpm (%s)' , gethostname() ); ?> " > /app/index.php
 
# ============================
# On va construire le livrable
# ============================
# ARG permet d'utiliser le même dockerfile pour httpd et pour phpfpm
#  + on récupère l'app déjà buildée (pour gagner le cache, on builde plus rapidement)
FROM ${BASE_IMAGE}
COPY --from=appbuilder --chown=www-data:www-data /app  /var/www/html

```


## Environnement de développement - rootless


### Identité / Permisisons

Le conteneur peut être lancé avec une identité arbitraire.

Par défaut, si le conteneur est lancé en tant que root, les processus Apache auront l'identité `www-data:www-data (33:33)`.

La directive `user: uuu:ggg` de `docker compose` ou la directive `--user` de `docker container run` permettent de choisir une identité arbitraire (qui n'est pas nécessairement existante dans le conteneur, ni sur l'hôte) telle qu'on l'entend dans la clause `user: "1971:1976"` d'un fichier `compose.yaml`. Tous les processus du conteneur auront alors cette idéndité, qui doit donc poséder les droits de lecture/écriture nécessaires au bon fonctionnement de l'application.

Si la variable **FIX_WEBCONTENT_OWNER** est positionnée à **true**, le propriétaire et le groupe des fichiers présents dans /var/www/html sera positionné à **www-data:www-data** (ou à l'identité choise au lancement du conteneur) pour assurer le droit de lecture/écriture par le serveur HTTP. Cette variable n'est pas définie par défaut.


### Identité du pool

- sur l'image officielle PHP/Debian, on est en standard sur un user/group (www-data:33)
- sur l'image officielle PHP/Alpine, on est en standard sur un user/group (www-data:82)

Dans ces images, le pool PHP-FPM est sur toutes les variantes configuré pour fonctionner en tant que user **www-data** (**uid=33**) et groupe **www-data** (**gid=33**) si le conteneur est lancé avec l'identité de root (par défaut), et les directives `User` et `Group` sont retirées de la configuration du pool dans le cas où le conteneur est lancé sous une identité arbitraire. Ainsi, les processus php-fpm ont l'identité arbitraire choisie.


### Utilisateur "de développement"

Pour construire des images de DEV basées sur celle-ci, mais devant tourner avec un autre UID que "www-data", on peut procéder via sudo, c'est l'approche initiale utilisée par cette image.

En effet, si la variable **USE_SUDO** vaut **true**, alors, le conteneur teste la présence de "sudo", et s'il est présent, exécute tous les scripts d'entrypoint via sudo, pour autoriser les ajustements de configuration même si le conteneur tourne sous une autre identité.


> Aujourd'hui l'image permet un lancement sous une identité arbitraire, ce qui règle le problème plus simplement. L'approche **sudo** pourrait désormais être retirée dans une version future.

Exemple de fichier "Dockerfile.userlocal"

````
# Pour autoriser des images de DEV basées sur celles-ci à tourner avec un autre UID, via sudo
ARG IMG
FROM ${IMG}
## Add local user
ARG USER_NAME
ARG USER_UID
RUN useradd ${USER_NAME} --uid ${USER_UID}
RUN set -eux \
 && if [ -f /etc/centos-release ] ; then yum -y install sudo && yum clean all ; fi \
 && if [ -f /etc/alpine-release ] ; then apk add --no-cache sudo ; fi \
 && if [ -f /etc/debian_version ] ; then apt-get update && apt-get -y install sudo && apt-get -y clean ; fi \
 && echo "${USER_NAME} ALL=(ALL) NOPASSWD:  ALL" > /etc/sudoers.d/localuser

ENV USER_NAME ${USER_NAME}
ENV USE_SUDO true
````


Exemple de lancement :

```
$ cat Makefile 
IMAGE=phpfpm
BASEIMAGE=registry.actilis.net/docker-images/phpfpm:8.0-docker-alpine
LOCALUSER_NAME=fmicaux
LOCALUSER_UID=1000


localuserbuild: ## service PHPFPM dans image de dev en tant que user local
localuserbuild: 
	tar c Dockerfile.localuser | docker image build -t $(IMAGE):localuser --build-arg IMG=$(BASEIMAGE) --build-arg USER_NAME=$(LOCALUSER_NAME) --build-arg USER_UID=$(LOCALUSER_UID) -f Dockerfile.localuser -


localuser: ## service PHPFPM dans image de dev en tant que user local
localuser: localuserbuild
	docker container run --name ${IMAGE}-test --user $(LOCALUSER_NAME) --rm -dit $(IMAGE):localuser 
	docker container logs -f ${IMAGE}-test

stop:
	@docker container kill ${IMAGE}-test || true



$ make localuser
```



# Changements récents

2023-02-23 : Passage à docker compose v2 sur toutes les images.
- docker-compose.yml -> compose.yaml
- docker-compose -> docker compose
